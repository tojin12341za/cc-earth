/////////////////////////////////
// CovidManagerHUD
//
// Copyright 2020 Carl Fravel
//

@EventConstructor()
export class CovidManager_tilt {
    /**
     * @param bUp true ==> tilt the globe toward vertical
     * */
    constructor(public bUp:boolean) {}
}

@EventConstructor()
export class CovidManager_rotate {
    /**
     * @param bLeftwise
     */
    constructor(public bLeftwise:boolean) {}
}

export class CovidManagerHUD {

    canvas:UICanvas = null
    uiMinimizedContainer: UIContainerRect
    uiMaximizedContainer: UIContainerRect
    uiMaximized:boolean = false
    maximizeButton:UIImage
    upButton:UIImage
    downButton:UIImage
    selectedItem:UIText
    clipboardImage:UIImage
    clipboardText:UIText
    leftButton:UIImage
    rightButton:UIImage
    actuateButton:UIImage
    minimizeButton:UIImage
    

    constructor (public eventListener:EventManager) {
        this.setupUI()
    }

    setupUI (){
        const HudLowerRightX = 0
        const HudLowerRightY = 200 
        const MinHudWidth = 50
        const MinHudHeight = 70
        const MaxHudWidth = 300
        const MaxHudHeight = 400
        //const hudBackgroundColor = new Color4(0.5,.00,.00, 1)
        const hudBackgroundColor = new Color4(0,.25,0, 1)
        // load the image atlases
        let clipboardAtlas = "materials/clipboard.0.754_256x256.jpg"
        let gameUiAtlas = "materials/game-atlas.png"
        let clipboardTexture = new Texture(clipboardAtlas)
        let gameUiTexture = new Texture(gameUiAtlas)

        // Create canvas component
        this.canvas = new UICanvas()
        this.canvas.hAlign = 'center'
        this.canvas.vAlign = 'bottom'
        
        //////////////////////// 
        // Minimized UI
        ////////////////////////

        // Container
        this.uiMinimizedContainer = new UIContainerRect(this.canvas)
        this.uiMinimizedContainer.hAlign = 'right'
        this.uiMinimizedContainer.vAlign = 'bottom'
        //this.uiMinimizedContainer.adaptHeight = true
        //this.uiMinimizedContainer.adaptWidth = true
        this.uiMinimizedContainer.width = 50
        this.uiMinimizedContainer.height = 70
        this.uiMinimizedContainer.positionY = 200
        this.uiMinimizedContainer.positionX = 0
        this.uiMinimizedContainer.color = hudBackgroundColor
        //this.uiMinimizedContainer.stackOrientation = UIStackOrientation.VERTICAL

        // Label
        const minimizedLabel = new UIText(this.uiMinimizedContainer)
        minimizedLabel.value = 'Covid-19\nInfo'
        minimizedLabel.color = Color4.White()
        minimizedLabel.hAlign = 'center'
        minimizedLabel.vAlign = 'bottom'
        //minimizedLabel.hTextAlign = 'center'
        minimizedLabel.paddingLeft = 7
        minimizedLabel.paddingTop = 0
        minimizedLabel.paddingBottom = 5
        minimizedLabel.fontSize = 8
        minimizedLabel.fontWeight = 'bold'
        minimizedLabel.isPointerBlocker = false

        // Maximize UI
        this.maximizeButton = new UIImage(this.uiMinimizedContainer, gameUiTexture)
        this.maximizeButton.sourceLeft = 496
        this.maximizeButton.sourceTop = 128
        this.maximizeButton.sourceWidth = 128
        this.maximizeButton.sourceHeight = 128
        this.maximizeButton.hAlign = 'right'
        this.maximizeButton.vAlign = 'bottom'
        this.maximizeButton.positionX = -5
        this.maximizeButton.positionY = 25
        this.maximizeButton.width=40
        this.maximizeButton.height=40
        this.maximizeButton.isPointerBlocker = true
        this.maximizeButton.onClick = new OnClick(() => {
            this.maximizeUI()
        }) 

        //////////////////////// 
        // Maximized UI
        ///////////////////////

        // Container       
        this.uiMaximizedContainer = new UIContainerRect(this.canvas)
        this.uiMaximizedContainer.hAlign = 'right'
        this.uiMaximizedContainer.vAlign = 'bottom'
        //this.uiMaximizedContainer.adaptWidth = true
        //this.uiMaximizedContainer.adaptHeight = true
        this.uiMaximizedContainer.width = 276
        this.uiMaximizedContainer.height = 445
        this.uiMaximizedContainer.positionX = 0
        this.uiMaximizedContainer.positionY = 200
        this.uiMaximizedContainer.color = hudBackgroundColor
        //this.uiMaximizedContainer.stackOrientation = UIStackOrientation.VERTICAL

        //Clipboard background image
        this.clipboardImage = new UIImage(this.uiMaximizedContainer, clipboardTexture)
        this.clipboardImage.sourceLeft = 0
        this.clipboardImage.sourceTop = 0
        this.clipboardImage.sourceWidth = 256
        this.clipboardImage.sourceHeight = 256
        this.clipboardImage.hAlign = 'center'
        this.clipboardImage.vAlign = 'center'
        this.clipboardImage.positionX = 0
        this.clipboardImage.positionY = 30
        this.clipboardImage.width=256
        this.clipboardImage.height=256/0.74
        this.clipboardImage.isPointerBlocker = false      
        
        // Cliboard Text
        this.clipboardText = new UIText(this.clipboardImage)
        this.clipboardText.value = 
        '\n\
        COVID-19 INFO\n\
        \n\
        Country:\n\
        Population:\n\
        Covid-19 Cases:\n\
        Covid-19 Deaths:\n\
        % Case Deaths:\n\
        Updated:\n\
        \n\
        Click nation dot for info.\n\
          Red = high death rate\n\
          Violet = low death rate\n\
          Black = incomplete data\n\
        \n\
        Press ESC to use this UI.'
        this.clipboardText.color = Color4.Black()
        this.clipboardText.hAlign = 'left'
        this.clipboardText.vAlign = 'bottom'
        this.clipboardText.paddingLeft = 25
        this.clipboardText.paddingTop = 0
        this.clipboardText.paddingBottom = 40
        this.clipboardText.fontSize = 8
        this.clipboardText.fontWeight = 'bold'
        // this.clipboardText.positionX = 40
        // this.clipboardText.positionY = 40
        this.clipboardText.fontSize = 14
        this.clipboardText.fontWeight = 'bold'
        this.clipboardText.isPointerBlocker = false

        // Button Labals
        const buttonLabels = new UIText(this.uiMaximizedContainer)
        //buttonLabels.value = 'Rotate                      Tilt'
        buttonLabels.value = '---Rotate---          ----Tilt-----'
        buttonLabels.color = Color4.White()
        buttonLabels.hAlign = 'left'
        buttonLabels.vAlign = 'bottom'
        buttonLabels.paddingTop = 0
        buttonLabels.paddingBottom = 62
        buttonLabels.paddingLeft = 63
        buttonLabels.fontSize = 10
        buttonLabels.fontWeight = 'bold'
        buttonLabels.isPointerBlocker = false

        // Rotate left
        this.leftButton = new UIImage(this.uiMaximizedContainer, gameUiTexture)
        this.leftButton.sourceLeft = 16
        this.leftButton.sourceTop = 275
        this.leftButton.sourceWidth = 76
        this.leftButton.sourceHeight = 76
        this.leftButton.hAlign = 'center'
        this.leftButton.vAlign = 'bottom'
        this.leftButton.positionX = -60
        this.leftButton.positionY = 30
        this.leftButton.width=30
        this.leftButton.height=30
        this.leftButton.isPointerBlocker = true
        this.leftButton.onClick = new OnClick(() => {
            this.eventListener.fireEvent(new CovidManager_rotate(true))
        })

        // Rotate right
        this.rightButton = new UIImage(this.uiMaximizedContainer, gameUiTexture)
        this.rightButton.sourceLeft = 95
        this.rightButton.sourceTop = 275
        this.rightButton.sourceWidth = 76
        this.rightButton.sourceHeight = 76
        this.rightButton.hAlign = 'center'
        this.rightButton.vAlign = 'bottom'
        this.rightButton.positionX = -25
        this.rightButton.positionY = 30
        this.rightButton.width=30
        this.rightButton.height=30
        this.rightButton.isPointerBlocker = true
        this.rightButton.onClick = new OnClick(() => {
            this.eventListener.fireEvent(new CovidManager_rotate(false))
        })

        // Tilt up
        this.upButton = new UIImage(this.uiMaximizedContainer, gameUiTexture)
        this.upButton.sourceLeft = 174
        this.upButton.sourceTop = 275
        this.upButton.sourceWidth = 76
        this.upButton.sourceHeight = 76
        this.upButton.hAlign = 'center'
        this.upButton.vAlign = 'bottom'
        this.upButton.positionX = 25
        this.upButton.positionY = 30
        this.upButton.width=30
        this.upButton.height=30
        this.upButton.isPointerBlocker = true
        this.upButton.onClick = new OnClick(() => {
            this.eventListener.fireEvent(new CovidManager_tilt(true))
        })

        // Tilt down
        this.downButton = new UIImage(this.uiMaximizedContainer, gameUiTexture)
        this.downButton.sourceLeft = 253
        this.downButton.sourceTop = 275
        this.downButton.sourceWidth = 76
        this.downButton.sourceHeight = 76
        this.downButton.hAlign = 'center'
        this.downButton.vAlign = 'bottom'
        this.downButton.positionX = 60
        this.downButton.positionY = 30
        this.downButton.width=30
        this.downButton.height=30
        this.downButton.isPointerBlocker = true
        this.downButton.onClick = new OnClick(() => {
            this.eventListener.fireEvent(new CovidManager_tilt(false))
        })

        // Minimize UI
        this.minimizeButton = new UIImage(this.uiMaximizedContainer, gameUiTexture)
        this.minimizeButton.sourceLeft = 496
        this.minimizeButton.sourceTop = 128
        this.minimizeButton.sourceWidth = 128
        this.minimizeButton.sourceHeight = 128
        this.minimizeButton.hAlign = 'right'
        this.minimizeButton.vAlign = 'bottom'
        this.minimizeButton.positionX = -5
        this.minimizeButton.positionY = 25
        this.minimizeButton.width=40
        this.minimizeButton.height=40
        this.minimizeButton.isPointerBlocker = true
        this.minimizeButton.onClick = new OnClick(() => {
             this.minimizeUI()
        })

        // HUD Caption
        const maximizedLabel = new UIText(this.uiMaximizedContainer)
        maximizedLabel.value = 'Covid-19 Info'
        maximizedLabel.color = Color4.White()
        maximizedLabel.hAlign = 'center'
        maximizedLabel.vAlign = 'bottom'
        maximizedLabel.paddingTop = 0
        maximizedLabel.paddingBottom = 5
        maximizedLabel.paddingLeft = 15
        maximizedLabel.fontSize = 14
        maximizedLabel.fontWeight = 'bold'
        maximizedLabel.isPointerBlocker = false

        // Now that it is all set up, minimize it
        this.minimizeUI()
    }


    maximizeUI(){
        this.uiMinimizedContainer.visible = false
        this.uiMaximizedContainer.visible = true
        this.uiMaximized = true
    }
    minimizeUI(){
        this.uiMaximizedContainer.visible = false
        this.uiMinimizedContainer.visible = true
        this.uiMaximized = false
    }
    setText(text:string) {
        this.clipboardText.value = text
        this.maximizeUI()
    }
}

