
export type countryDataType =
{
  "country"?: string,
  "latitude"?: number,
  "longitude"?: number,
  "name"?: string,
  "Country Name"?: string,
  "Country Code"?: string
}

export const countryData:countryDataType[] = [
   {
      "country": "AF",
      "latitude": 33.93911,
      "longitude": 67.709953,
      "name": "Afghanistan",
      "Country Name": "Afghanistan",
      "Country Code": "AFG"
   },
   {
      "country": "AL",
      "latitude": 41.153332,
      "longitude": 20.168331,
      "name": "Albania",
      "Country Name": "Albania",
      "Country Code": "ALB"
   },
   {
      "country": "DZ",
      "latitude": 28.033886,
      "longitude": 1.659626,
      "name": "Algeria",
      "Country Name": "Algeria",
      "Country Code": "DZA"
   },
   {
      "country": "AS",
      "latitude": -14.270972,
      "longitude": -170.132217,
      "name": "American Samoa",
      "Country Name": "American Samoa",
      "Country Code": "ASM"
   },
   {
      "country": "AD",
      "latitude": 42.546245,
      "longitude": 1.601554,
      "name": "Andorra",
      "Country Name": "Andorra",
      "Country Code": "AND"
   },
   {
      "country": "AO",
      "latitude": -11.202692,
      "longitude": 17.873887,
      "name": "Angola",
      "Country Name": "Angola",
      "Country Code": "AGO"
   },
   {
      "country": "AI",
      "latitude": 18.220554,
      "longitude": -63.068615,
      "name": "Anguilla"
   },
   {
      "country": "AQ",
      "latitude": -75.250973,
      "longitude": -0.071389,
      "name": "Antarctica"
   },
   {
      "country": "AG",
      "latitude": 17.060816,
      "longitude": -61.796428,
      "name": "Antigua and Barbuda",
      "Country Name": "Antigua and Barbuda",
      "Country Code": "ATG"
   },
   {
      "country": "AR",
      "latitude": -38.416097,
      "longitude": -63.616672,
      "name": "Argentina",
      "Country Name": "Argentina",
      "Country Code": "ARG"
   },
   {
      "country": "AM",
      "latitude": 40.069099,
      "longitude": 45.038189,
      "name": "Armenia",
      "Country Name": "Armenia",
      "Country Code": "ARM"
   },
   {
      "country": "AW",
      "latitude": 12.52111,
      "longitude": -69.968338,
      "name": "Aruba",
      "Country Name": "Aruba",
      "Country Code": "ABW"
   },
   {
      "country": "AU",
      "latitude": -25.274398,
      "longitude": 133.775136,
      "name": "Australia",
      "Country Name": "Australia",
      "Country Code": "AUS"
   },
   {
      "country": "AT",
      "latitude": 47.516231,
      "longitude": 14.550072,
      "name": "Austria",
      "Country Name": "Austria",
      "Country Code": "AUT"
   },
   {
      "country": "AZ",
      "latitude": 40.143105,
      "longitude": 47.576927,
      "name": "Azerbaijan",
      "Country Name": "Azerbaijan",
      "Country Code": "AZE"
   },
   {
      "country": "BS",
      "latitude": 25.03428,
      "longitude": -77.39628,
      "name": "Bahamas",
      "Country Name": "The Bahamas",
      "Country Code": "BHS"
   },
   {
      "country": "BH",
      "latitude": 25.930414,
      "longitude": 50.637772,
      "name": "Bahrain",
      "Country Name": "Bahrain",
      "Country Code": "BHR"
   },
   {
      "country": "BD",
      "latitude": 23.684994,
      "longitude": 90.356331,
      "name": "Bangladesh",
      "Country Name": "Bangladesh",
      "Country Code": "BGD"
   },
   {
      "country": "BB",
      "latitude": 13.193887,
      "longitude": -59.543198,
      "name": "Barbados",
      "Country Name": "Barbados",
      "Country Code": "BRB"
   },
   {
      "country": "BY",
      "latitude": 53.709807,
      "longitude": 27.953389,
      "name": "Belarus",
      "Country Name": "Belarus",
      "Country Code": "BLR"
   },
   {
      "country": "BE",
      "latitude": 50.503887,
      "longitude": 4.469936,
      "name": "Belgium",
      "Country Name": "Belgium",
      "Country Code": "BEL"
   },
   {
      "country": "BZ",
      "latitude": 17.189877,
      "longitude": -88.49765,
      "name": "Belize",
      "Country Name": "Belize",
      "Country Code": "BLZ"
   },
   {
      "country": "BJ",
      "latitude": 9.30769,
      "longitude": 2.315834,
      "name": "Benin",
      "Country Name": "Benin",
      "Country Code": "BEN"
   },
   {
      "country": "BM",
      "latitude": 32.321384,
      "longitude": -64.75737,
      "name": "Bermuda",
      "Country Name": "Bermuda",
      "Country Code": "BMU"
   },
   {
      "country": "BT",
      "latitude": 27.514162,
      "longitude": 90.433601,
      "name": "Bhutan",
      "Country Name": "Bhutan",
      "Country Code": "BTN"
   },
   {
      "country": "BO",
      "latitude": -16.290154,
      "longitude": -63.588653,
      "name": "Bolivia",
      "Country Name": "Bolivia",
      "Country Code": "BOL"
   },
   {
      "country": "BA",
      "latitude": 43.915886,
      "longitude": 17.679076,
      "name": "Bosnia and Herzegovina",
      "Country Name": "Bosnia and Herzegovina",
      "Country Code": "BIH"
   },
   {
      "country": "BW",
      "latitude": -22.328474,
      "longitude": 24.684866,
      "name": "Botswana",
      "Country Name": "Botswana",
      "Country Code": "BWA"
   },
   {
      "country": "BR",
      "latitude": -14.235004,
      "longitude": -51.92528,
      "name": "Brazil",
      "Country Name": "Brazil",
      "Country Code": "BRA"
   },
   {
      "country": "IO",
      "latitude": -6.343194,
      "longitude": 71.876519,
      "name": "British Indian Ocean Territory"
   },
   {
      "country": "VG",
      "latitude": 18.420695,
      "longitude": -64.639968,
      "name": "British Virgin Islands",
      "Country Name": "British Virgin Islands",
      "Country Code": "VGB"
   },
   {
      "country": "BN",
      "latitude": 4.535277,
      "longitude": 114.727669,
      "name": "Brunei",
      "Country Name": "Brunei Darussalam",
      "Country Code": "BRN"
   },
   {
      "country": "BG",
      "latitude": 42.733883,
      "longitude": 25.48583,
      "name": "Bulgaria",
      "Country Name": "Bulgaria",
      "Country Code": "BGR"
   },
   {
      "country": "BF",
      "latitude": 12.238333,
      "longitude": -1.561593,
      "name": "Burkina Faso",
      "Country Name": "Burkina Faso",
      "Country Code": "BFA"
   },
   {
      "country": "BI",
      "latitude": -3.373056,
      "longitude": 29.918886,
      "name": "Burundi",
      "Country Name": "Burundi",
      "Country Code": "BDI"
   },
   {
      "Country Name": "Cabo Verde",
      "Country Code": "CPV"
   },
   {
      "country": "KH",
      "latitude": 12.565679,
      "longitude": 104.990963,
      "name": "Cambodia",
      "Country Name": "Cambodia",
      "Country Code": "KHM"
   },
   {
      "country": "CM",
      "latitude": 7.369722,
      "longitude": 12.354722,
      "name": "Cameroon",
      "Country Name": "Cameroon",
      "Country Code": "CMR"
   },
   {
      "country": "CA",
      "latitude": 56.130366,
      "longitude": -106.346771,
      "name": "Canada",
      "Country Name": "Canada",
      "Country Code": "CAN"
   },
   {
      "country": "CV",
      "latitude": 16.002082,
      "longitude": -24.013197,
      "name": "Cape Verde"
   },
   {
      "Country Name": "Caribbean small states",
      "Country Code": "CSS"
   },
   {
      "country": "KY",
      "latitude": 19.513469,
      "longitude": -80.566956,
      "name": "Cayman Islands",
      "Country Name": "Cayman Islands",
      "Country Code": "CYM"
   },
   {
      "country": "CF",
      "latitude": 6.611111,
      "longitude": 20.939444,
      "name": "Central African Republic",
      "Country Name": "Central African Republic",
      "Country Code": "CAF"
   },
   {
      "country": "TD",
      "latitude": 15.454166,
      "longitude": 18.732207,
      "name": "Chad",
      "Country Name": "Chad",
      "Country Code": "TCD"
   },
   {
      "country": "CL",
      "latitude": -35.675147,
      "longitude": -71.542969,
      "name": "Chile",
      "Country Name": "Chile",
      "Country Code": "CHL"
   },
   {
      "country": "CN",
      "latitude": 35.86166,
      "longitude": 104.195397,
      "name": "China",
      "Country Name": "China",
      "Country Code": "CHN"
   },
   {
      "country": "CO",
      "latitude": 4.570868,
      "longitude": -74.297333,
      "name": "Colombia",
      "Country Name": "Colombia",
      "Country Code": "COL"
   },
   {
      "country": "KM",
      "latitude": -11.875001,
      "longitude": 43.872219,
      "name": "Comoros",
      "Country Name": "Comoros",
      "Country Code": "COM"
   },
   {
      "country": "CD",
      "latitude": -4.038333,
      "longitude": 21.758664,
      "name": "Congo [DRC]",
      "Country Name": "Congo, Dem. Rep.",
      "Country Code": "COD"
   },
   {
      "country": "CG",
      "latitude": -0.228021,
      "longitude": 15.827659,
      "name": "Congo [Republic]",
      "Country Name": "Congo, Rep.",
      "Country Code": "COG"
   },
   {
      "country": "CR",
      "latitude": 9.748917,
      "longitude": -83.753428,
      "name": "Costa Rica",
      "Country Name": "Costa Rica",
      "Country Code": "CRI"
   },
   {
      "country": "CI",
      "latitude": 7.539989,
      "longitude": -5.54708,
      "name": "Côte d'Ivoire",
      "Country Name": "Cote d'Ivoire",
      "Country Code": "CIV"
   },
   {
      "country": "HR",
      "latitude": 45.1,
      "longitude": 15.2,
      "name": "Croatia",
      "Country Name": "Croatia",
      "Country Code": "HRV"
   },
   {
      "country": "CU",
      "latitude": 21.521757,
      "longitude": -77.781167,
      "name": "Cuba",
      "Country Name": "Cuba",
      "Country Code": "CUB"
   },
   {
      "latitude": 12.12,
      "longitude": -68.93,
      "name": "Curacao",
      "Country Name": "Curacao",
      "Country Code": "CUW"
   },
   {
      "country": "CY",
      "latitude": 35.126413,
      "longitude": 33.429859,
      "name": "Cyprus",
      "Country Name": "Cyprus",
      "Country Code": "CYP"
   },
   {
      "country": "CZ",
      "latitude": 49.817492,
      "longitude": 15.472962,
      "name": "Czech Republic",
      "Country Name": "Czech Republic",
      "Country Code": "CZE"
   },
   {
      "country": "DK",
      "latitude": 56.26392,
      "longitude": 9.501785,
      "name": "Denmark",
      "Country Name": "Denmark",
      "Country Code": "DNK"
   },
   {
      "country": "DJ",
      "latitude": 11.825138,
      "longitude": 42.590275,
      "name": "Djibouti",
      "Country Name": "Djibouti",
      "Country Code": "DJI"
   },
   {
      "country": "DM",
      "latitude": 15.414999,
      "longitude": -61.370976,
      "name": "Dominica",
      "Country Name": "Dominica",
      "Country Code": "DMA"
   },
   {
      "country": "DO",
      "latitude": 18.735693,
      "longitude": -70.162651,
      "name": "Dominican Republic",
      "Country Name": "Dominican Republic",
      "Country Code": "DOM"
   },
   {
      "country": "EC",
      "latitude": -1.831239,
      "longitude": -78.183406,
      "name": "Ecuador",
      "Country Name": "Ecuador",
      "Country Code": "ECU"
   },
   {
      "country": "EG",
      "latitude": 26.820553,
      "longitude": 30.802498,
      "name": "Egypt",
      "Country Name": "Egypt",
      "Country Code": "EGY"
   },
   {
      "country": "SV",
      "latitude": 13.794185,
      "longitude": -88.89653,
      "name": "El Salvador",
      "Country Name": "El Salvador",
      "Country Code": "SLV"
   },
   {
      "country": "GQ",
      "latitude": 1.650801,
      "longitude": 10.267895,
      "name": "Equatorial Guinea",
      "Country Name": "Equatorial Guinea",
      "Country Code": "GNQ"
   },
   {
      "country": "ER",
      "latitude": 15.179384,
      "longitude": 39.782334,
      "name": "Eritrea",
      "Country Name": "Eritrea",
      "Country Code": "ERI"
   },
   {
      "country": "EE",
      "latitude": 58.595272,
      "longitude": 25.013607,
      "name": "Estonia",
      "Country Name": "Estonia",
      "Country Code": "EST"
   },
   {
      "country": "SZ",
      "latitude": -26.522503,
      "longitude": 31.465866,
      "name": "Swaziland",
      "Country Name": "Eswatini",
      "Country Code": "SWZ"
   },
   {
      "country": "ET",
      "latitude": 9.145,
      "longitude": 40.489673,
      "name": "Ethiopia",
      "Country Name": "Ethiopia",
      "Country Code": "ETH"
   },
   {
      "country": "FK",
      "latitude": -51.796253,
      "longitude": -59.523613,
      "name": "Falkland Islands [Islas Malvinas]",
      "Country Name": "Falkland Islands",
      "Country Code": "FLK"
   },
   {
      "country": "FO",
      "latitude": 61.892635,
      "longitude": -6.911806,
      "name": "Faroe Islands",
      "Country Name": "Faroe Islands",
      "Country Code": "FRO"
   },
   {
      "country": "FJ",
      "latitude": -16.578193,
      "longitude": 179.414413,
      "name": "Fiji",
      "Country Name": "Fiji",
      "Country Code": "FJI"
   },
   {
      "country": "FI",
      "latitude": 61.92411,
      "longitude": 25.748151,
      "name": "Finland",
      "Country Name": "Finland",
      "Country Code": "FIN"
   },
   {
      "country": "FR",
      "latitude": 46.227638,
      "longitude": 2.213749,
      "name": "France",
      "Country Name": "France",
      "Country Code": "FRA"
   },
   {
      "country": "GF",
      "latitude": 3.933889,
      "longitude": -53.125782,
      "name": "French Guiana"
   },
   {
      "country": "PF",
      "latitude": -17.679742,
      "longitude": -149.406843,
      "name": "French Polynesia",
      "Country Name": "French Polynesia",
      "Country Code": "PYF"
   },
   {
      "country": "TF",
      "latitude": -49.280366,
      "longitude": 69.348557,
      "name": "French Southern Territories"
   },
   {
      "country": "GA",
      "latitude": -0.803689,
      "longitude": 11.609444,
      "name": "Gabon",
      "Country Name": "Gabon",
      "Country Code": "GAB"
   },
   {
      "country": "GM",
      "latitude": 13.443182,
      "longitude": -15.310139,
      "name": "Gambia",
      "Country Name": "Gambia",
      "Country Code": "GMB"
   },
   {
      "country": "GZ",
      "latitude": 31.354676,
      "longitude": 34.308825,
      "name": "Gaza Strip"
   },
   {
      "country": "GE",
      "latitude": 42.315407,
      "longitude": 43.356892,
      "name": "Georgia",
      "Country Name": "Georgia",
      "Country Code": "GEO"
   },
   {
      "country": "DE",
      "latitude": 51.165691,
      "longitude": 10.451526,
      "name": "Germany",
      "Country Name": "Germany",
      "Country Code": "DEU"
   },
   {
      "country": "GH",
      "latitude": 7.946527,
      "longitude": -1.023194,
      "name": "Ghana",
      "Country Name": "Ghana",
      "Country Code": "GHA"
   },
   {
      "country": "GI",
      "latitude": 36.137741,
      "longitude": -5.345374,
      "name": "Gibraltar",
      "Country Name": "Gibraltar",
      "Country Code": "GIB"
   },
   {
      "country": "GR",
      "latitude": 39.074208,
      "longitude": 21.824312,
      "name": "Greece",
      "Country Name": "Greece",
      "Country Code": "GRC"
   },
   {
      "country": "GL",
      "latitude": 71.706936,
      "longitude": -42.604303,
      "name": "Greenland",
      "Country Name": "Greenland",
      "Country Code": "GRL"
   },
   {
      "country": "GD",
      "latitude": 12.262776,
      "longitude": -61.604171,
      "name": "Grenada",
      "Country Name": "Grenada",
      "Country Code": "GRD"
   },
   {
      "country": "GP",
      "latitude": 16.995971,
      "longitude": -62.067641,
      "name": "Guadeloupe"
   },
   {
      "country": "GU",
      "latitude": 13.444304,
      "longitude": 144.793731,
      "name": "Guam",
      "Country Name": "Guam",
      "Country Code": "GUM"
   },
   {
      "country": "GT",
      "latitude": 15.783471,
      "longitude": -90.230759,
      "name": "Guatemala",
      "Country Name": "Guatemala",
      "Country Code": "GTM"
   },
   {
      "country": "GG",
      "latitude": 49.465691,
      "longitude": -2.585278,
      "name": "Guernsey"
   },
   {
      "country": "GN",
      "latitude": 9.945587,
      "longitude": -9.696645,
      "name": "Guinea",
      "Country Name": "Guinea",
      "Country Code": "GIN"
   },
   {
      "country": "GW",
      "latitude": 11.803749,
      "longitude": -15.180413,
      "name": "Guinea-Bissau",
      "Country Name": "Guinea-Bissau",
      "Country Code": "GNB"
   },
   {
      "country": "GY",
      "latitude": 4.860416,
      "longitude": -58.93018,
      "name": "Guyana",
      "Country Name": "Guyana",
      "Country Code": "GUY"
   },
   {
      "country": "HT",
      "latitude": 18.971187,
      "longitude": -72.285215,
      "name": "Haiti",
      "Country Name": "Haiti",
      "Country Code": "HTI"
   },
   {
      "country": "HM",
      "latitude": -53.08181,
      "longitude": 73.504158,
      "name": "Heard Island and McDonald Islands"
   },
   {
      "country": "HN",
      "latitude": 15.199999,
      "longitude": -86.241905,
      "name": "Honduras",
      "Country Name": "Honduras",
      "Country Code": "HND"
   },
   {
      "country": "HK",
      "latitude": 22.396428,
      "longitude": 114.109497,
      "name": "Hong Kong",
      "Country Name": "Hong Kong SAR, China",
      "Country Code": "HKG"
   },
   {
      "country": "HU",
      "latitude": 47.162494,
      "longitude": 19.503304,
      "name": "Hungary",
      "Country Name": "Hungary",
      "Country Code": "HUN"
   },
   {
      "country": "IS",
      "latitude": 64.963051,
      "longitude": -19.020835,
      "name": "Iceland",
      "Country Name": "Iceland",
      "Country Code": "ISL"
   },
   {
      "country": "IN",
      "latitude": 20.593684,
      "longitude": 78.96288,
      "name": "India",
      "Country Name": "India",
      "Country Code": "IND"
   },
   {
      "country": "ID",
      "latitude": -0.789275,
      "longitude": 113.921327,
      "name": "Indonesia",
      "Country Name": "Indonesia",
      "Country Code": "IDN"
   },
   {
      "country": "IR",
      "latitude": 32.427908,
      "longitude": 53.688046,
      "name": "Iran",
      "Country Name": "Iran",
      "Country Code": "IRN"
   },
   {
      "country": "IQ",
      "latitude": 33.223191,
      "longitude": 43.679291,
      "name": "Iraq",
      "Country Name": "Iraq",
      "Country Code": "IRQ"
   },
   {
      "country": "IE",
      "latitude": 53.41291,
      "longitude": -8.24389,
      "name": "Ireland",
      "Country Name": "Ireland",
      "Country Code": "IRL"
   },
   {
      "country": "IM",
      "latitude": 54.236107,
      "longitude": -4.548056,
      "name": "Isle of Man",
      "Country Name": "Isle of Man",
      "Country Code": "IMN"
   },
   {
      "country": "IL",
      "latitude": 31.046051,
      "longitude": 34.851612,
      "name": "Israel",
      "Country Name": "Israel",
      "Country Code": "ISR"
   },
   {
      "country": "IT",
      "latitude": 41.87194,
      "longitude": 12.56738,
      "name": "Italy",
      "Country Name": "Italy",
      "Country Code": "ITA"
   },
   {
      "country": "JM",
      "latitude": 18.109581,
      "longitude": -77.297508,
      "name": "Jamaica",
      "Country Name": "Jamaica",
      "Country Code": "JAM"
   },
   {
      "country": "JP",
      "latitude": 36.204824,
      "longitude": 138.252924,
      "name": "Japan",
      "Country Name": "Japan",
      "Country Code": "JPN"
   },
   {
      "country": "JE",
      "latitude": 49.214439,
      "longitude": -2.13125,
      "name": "Jersey"
   },
   {
      "country": "JO",
      "latitude": 30.585164,
      "longitude": 36.238414,
      "name": "Jordan",
      "Country Name": "Jordan",
      "Country Code": "JOR"
   },
   {
      "country": "KZ",
      "latitude": 48.019573,
      "longitude": 66.923684,
      "name": "Kazakhstan",
      "Country Name": "Kazakhstan",
      "Country Code": "KAZ"
   },
   {
      "country": "KE",
      "latitude": -0.023559,
      "longitude": 37.906193,
      "name": "Kenya",
      "Country Name": "Kenya",
      "Country Code": "KEN"
   },
   {
      "country": "KI",
      "latitude": -3.370417,
      "longitude": -168.734039,
      "name": "Kiribati",
      "Country Name": "Kiribati",
      "Country Code": "KIR"
   },
   {
      "country": "XK",
      "latitude": 42.602636,
      "longitude": 20.902977,
      "name": "Kosovo",
      "Country Name": "Kosovo",
      "Country Code": "XKX"
   },
   {
      "country": "KW",
      "latitude": 29.31166,
      "longitude": 47.481766,
      "name": "Kuwait",
      "Country Name": "Kuwait",
      "Country Code": "KWT"
   },
   {
      "country": "KG",
      "latitude": 41.20438,
      "longitude": 74.766098,
      "name": "Kyrgyzstan",
      "Country Name": "Kyrgyzstan",
      "Country Code": "KGZ"
   },
   {
      "country": "LA",
      "latitude": 19.85627,
      "longitude": 102.495496,
      "name": "Laos",
      "Country Name": "Laos",
      "Country Code": "LAO"
   },
   {
      "country": "LV",
      "latitude": 56.879635,
      "longitude": 24.603189,
      "name": "Latvia",
      "Country Name": "Latvia",
      "Country Code": "LVA"
   },
   {
      "country": "LB",
      "latitude": 33.854721,
      "longitude": 35.862285,
      "name": "Lebanon",
      "Country Name": "Lebanon",
      "Country Code": "LBN"
   },
   {
      "country": "LS",
      "latitude": -29.609988,
      "longitude": 28.233608,
      "name": "Lesotho",
      "Country Name": "Lesotho",
      "Country Code": "LSO"
   },
   {
      "country": "LR",
      "latitude": 6.428055,
      "longitude": -9.429499,
      "name": "Liberia",
      "Country Name": "Liberia",
      "Country Code": "LBR"
   },
   {
      "country": "LY",
      "latitude": 26.3351,
      "longitude": 17.228331,
      "name": "Libya",
      "Country Name": "Libya",
      "Country Code": "LBY"
   },
   {
      "country": "LI",
      "latitude": 47.166,
      "longitude": 9.555373,
      "name": "Liechtenstein",
      "Country Name": "Liechtenstein",
      "Country Code": "LIE"
   },
   {
      "country": "LT",
      "latitude": 55.169438,
      "longitude": 23.881275,
      "name": "Lithuania",
      "Country Name": "Lithuania",
      "Country Code": "LTU"
   },
   {
      "country": "LU",
      "latitude": 49.815273,
      "longitude": 6.129583,
      "name": "Luxembourg",
      "Country Name": "Luxembourg",
      "Country Code": "LUX"
   },
   {
      "country": "MO",
      "latitude": 22.198745,
      "longitude": 113.543873,
      "name": "Macau",
      "Country Name": "Macao",
      "Country Code": "MAC"
   },
   {
      "country": "MK",
      "latitude": 41.608635,
      "longitude": 21.745275,
      "name": "Macedonia [FYROM]"
   },
   {
      "country": "MG",
      "latitude": -18.766947,
      "longitude": 46.869107,
      "name": "Madagascar",
      "Country Name": "Madagascar",
      "Country Code": "MDG"
   },
   {
      "country": "MW",
      "latitude": -13.254308,
      "longitude": 34.301525,
      "name": "Malawi",
      "Country Name": "Malawi",
      "Country Code": "MWI"
   },
   {
      "country": "MY",
      "latitude": 4.210484,
      "longitude": 101.975766,
      "name": "Malaysia",
      "Country Name": "Malaysia",
      "Country Code": "MYS"
   },
   {
      "country": "MV",
      "latitude": 3.202778,
      "longitude": 73.22068,
      "name": "Maldives",
      "Country Name": "Maldives",
      "Country Code": "MDV"
   },
   {
      "country": "ML",
      "latitude": 17.570692,
      "longitude": -3.996166,
      "name": "Mali",
      "Country Name": "Mali",
      "Country Code": "MLI"
   },
   {
      "country": "MT",
      "latitude": 35.937496,
      "longitude": 14.375416,
      "name": "Malta",
      "Country Name": "Malta",
      "Country Code": "MLT"
   },
   {
      "country": "MH",
      "latitude": 7.131474,
      "longitude": 171.184478,
      "name": "Marshall Islands",
      "Country Name": "Marshall Islands",
      "Country Code": "MHL"
   },
   {
      "country": "MQ",
      "latitude": 14.641528,
      "longitude": -61.024174,
      "name": "Martinique"
   },
   {
      "country": "MR",
      "latitude": 21.00789,
      "longitude": -10.940835,
      "name": "Mauritania",
      "Country Name": "Mauritania",
      "Country Code": "MRT"
   },
   {
      "country": "MU",
      "latitude": -20.348404,
      "longitude": 57.552152,
      "name": "Mauritius",
      "Country Name": "Mauritius",
      "Country Code": "MUS"
   },
   {
      "country": "YT",
      "latitude": -12.8275,
      "longitude": 45.166244,
      "name": "Mayotte"
   },
   {
      "country": "MX",
      "latitude": 23.634501,
      "longitude": -102.552784,
      "name": "Mexico",
      "Country Name": "Mexico",
      "Country Code": "MEX"
   },
   {
      "country": "FM",
      "latitude": 7.425554,
      "longitude": 150.550812,
      "name": "Micronesia",
      "Country Name": "Micronesia",
      "Country Code": "FSM"
   },
   {
      "country": "MD",
      "latitude": 47.411631,
      "longitude": 28.369885,
      "name": "Moldova",
      "Country Name": "Moldova",
      "Country Code": "MDA"
   },
   {
      "country": "MC",
      "latitude": 43.750298,
      "longitude": 7.412841,
      "name": "Monaco",
      "Country Name": "Monaco",
      "Country Code": "MCO"
   },
   {
      "country": "MN",
      "latitude": 46.862496,
      "longitude": 103.846656,
      "name": "Mongolia",
      "Country Name": "Mongolia",
      "Country Code": "MNG"
   },
   {
      "country": "ME",
      "latitude": 42.708678,
      "longitude": 19.37439,
      "name": "Montenegro",
      "Country Name": "Montenegro",
      "Country Code": "MNE"
   },
   {
      "country": "MS",
      "latitude": 16.742498,
      "longitude": -62.187366,
      "name": "Montserrat"
   },
   {
      "country": "MA",
      "latitude": 31.791702,
      "longitude": -7.09262,
      "name": "Morocco",
      "Country Name": "Morocco",
      "Country Code": "MAR"
   },
   {
      "country": "MZ",
      "latitude": -18.665695,
      "longitude": 35.529562,
      "name": "Mozambique",
      "Country Name": "Mozambique",
      "Country Code": "MOZ"
   },
   {
      "country": "MM",
      "latitude": 21.913965,
      "longitude": 95.956223,
      "name": "Myanmar [Burma]",
      "Country Name": "Myanmar",
      "Country Code": "MMR"
   },
   {
      "country": "NA",
      "latitude": -22.95764,
      "longitude": 18.49041,
      "name": "Namibia",
      "Country Name": "Namibia",
      "Country Code": "NAM"
   },
   {
      "country": "NR",
      "latitude": -0.522778,
      "longitude": 166.931503,
      "name": "Nauru",
      "Country Name": "Nauru",
      "Country Code": "NRU"
   },
   {
      "country": "NP",
      "latitude": 28.394857,
      "longitude": 84.124008,
      "name": "Nepal",
      "Country Name": "Nepal",
      "Country Code": "NPL"
   },
   {
      "country": "NL",
      "latitude": 52.132633,
      "longitude": 5.291266,
      "name": "Netherlands",
      "Country Name": "Netherlands",
      "Country Code": "NLD"
   },
   {
      "country": "AN",
      "latitude": 12.226079,
      "longitude": -69.060087,
      "name": "Netherlands Antilles"
   },
   {
      "country": "NC",
      "latitude": -20.904305,
      "longitude": 165.618042,
      "name": "New Caledonia",
      "Country Name": "New Caledonia",
      "Country Code": "NCL"
   },
   {
      "country": "NZ",
      "latitude": -40.900557,
      "longitude": 174.885971,
      "name": "New Zealand",
      "Country Name": "New Zealand",
      "Country Code": "NZL"
   },
   {
      "country": "NI",
      "latitude": 12.865416,
      "longitude": -85.207229,
      "name": "Nicaragua",
      "Country Name": "Nicaragua",
      "Country Code": "NIC"
   },
   {
      "country": "NE",
      "latitude": 17.607789,
      "longitude": 8.081666,
      "name": "Niger",
      "Country Name": "Niger",
      "Country Code": "NER"
   },
   {
      "country": "NG",
      "latitude": 9.081999,
      "longitude": 8.675277,
      "name": "Nigeria",
      "Country Name": "Nigeria",
      "Country Code": "NGA"
   },
   {
      "country": "NU",
      "latitude": -19.054445,
      "longitude": -169.867233,
      "name": "Niue"
   },
   {
      "country": "NF",
      "latitude": -29.040835,
      "longitude": 167.954712,
      "name": "Norfolk Island"
   },
   {
      "country": "KP",
      "latitude": 40.339852,
      "longitude": 127.510093,
      "name": "North Korea",
      "Country Name": "North Korea",
      "Country Code": "PRK"
   },
   {
      "Country Name": "North Macedonia",
      "Country Code": "MKD"
   },
   {
      "country": "MP",
      "latitude": 17.33083,
      "longitude": 145.38469,
      "name": "Northern Mariana Islands",
      "Country Name": "Northern Mariana Islands",
      "Country Code": "MNP"
   },
   {
      "country": "NO",
      "latitude": 60.472024,
      "longitude": 8.468946,
      "name": "Norway",
      "Country Name": "Norway",
      "Country Code": "NOR"
   },
   {
      "country": "OM",
      "latitude": 21.512583,
      "longitude": 55.923255,
      "name": "Oman",
      "Country Name": "Oman",
      "Country Code": "OMN"
   },
   {
      "country": "PK",
      "latitude": 30.375321,
      "longitude": 69.345116,
      "name": "Pakistan",
      "Country Name": "Pakistan",
      "Country Code": "PAK"
   },
   {
      "country": "PW",
      "latitude": 7.51498,
      "longitude": 134.58252,
      "name": "Palau",
      "Country Name": "Palau",
      "Country Code": "PLW"
   },
   {
      "country": "PS",
      "latitude": 31.952162,
      "longitude": 35.233154,
      "name": "Palestinian Territories"
   },
   {
      "country": "PA",
      "latitude": 8.537981,
      "longitude": -80.782127,
      "name": "Panama",
      "Country Name": "Panama",
      "Country Code": "PAN"
   },
   {
      "country": "PG",
      "latitude": -6.314993,
      "longitude": 143.95555,
      "name": "Papua New Guinea",
      "Country Name": "Papua New Guinea",
      "Country Code": "PNG"
   },
   {
      "country": "PY",
      "latitude": -23.442503,
      "longitude": -58.443832,
      "name": "Paraguay",
      "Country Name": "Paraguay",
      "Country Code": "PRY"
   },
   {
      "country": "PE",
      "latitude": -9.189967,
      "longitude": -75.015152,
      "name": "Peru",
      "Country Name": "Peru",
      "Country Code": "PER"
   },
   {
      "country": "PH",
      "latitude": 12.879721,
      "longitude": 121.774017,
      "name": "Philippines",
      "Country Name": "Philippines",
      "Country Code": "PHL"
   },
   {
      "country": "PN",
      "latitude": -24.703615,
      "longitude": -127.439308,
      "name": "Pitcairn Islands"
   },
   {
      "country": "PL",
      "latitude": 51.919438,
      "longitude": 19.145136,
      "name": "Poland",
      "Country Name": "Poland",
      "Country Code": "POL"
   },
   {
      "country": "PT",
      "latitude": 39.399872,
      "longitude": -8.224454,
      "name": "Portugal",
      "Country Name": "Portugal",
      "Country Code": "PRT"
   },
   {
      "country": "PR",
      "latitude": 18.220833,
      "longitude": -66.590149,
      "name": "Puerto Rico",
      "Country Name": "Puerto Rico",
      "Country Code": "PRI"
   },
   {
      "country": "QA",
      "latitude": 25.354826,
      "longitude": 51.183884,
      "name": "Qatar",
      "Country Name": "Qatar",
      "Country Code": "QAT"
   },
   {
      "country": "RE",
      "latitude": -21.115141,
      "longitude": 55.536384,
      "name": "Réunion"
   },
   {
      "country": "RO",
      "latitude": 45.943161,
      "longitude": 24.96676,
      "name": "Romania",
      "Country Name": "Romania",
      "Country Code": "ROU"
   },
   {
      "country": "RU",
      "latitude": 61.52401,
      "longitude": 105.318756,
      "name": "Russia",
      "Country Name": "Russian Federation",
      "Country Code": "RUS"
   },
   {
      "country": "RW",
      "latitude": -1.940278,
      "longitude": 29.873888,
      "name": "Rwanda",
      "Country Name": "Rwanda",
      "Country Code": "RWA"
   },
   {
      "country": "SH",
      "latitude": -24.143474,
      "longitude": -10.030696,
      "name": "Saint Helena"
   },
   {
      "country": "KN",
      "latitude": 17.357822,
      "longitude": -62.782998,
      "name": "Saint Kitts and Nevis"
   },
   {
      "country": "LC",
      "latitude": 13.909444,
      "longitude": -60.978893,
      "name": "Saint Lucia"
   },
   {
      "country": "PM",
      "latitude": 46.941936,
      "longitude": -56.27111,
      "name": "Saint Pierre and Miquelon"
   },
   {
      "country": "VC",
      "latitude": 12.984305,
      "longitude": -61.287228,
      "name": "Saint Vincent and the Grenadines"
   },
   {
      "country": "WS",
      "latitude": -13.759029,
      "longitude": -172.104629,
      "name": "Samoa",
      "Country Name": "Samoa",
      "Country Code": "WSM"
   },
   {
      "country": "SM",
      "latitude": 43.94236,
      "longitude": 12.457777,
      "name": "San Marino",
      "Country Name": "San Marino",
      "Country Code": "SMR"
   },
   {
      "country": "ST",
      "latitude": 0.18636,
      "longitude": 6.613081,
      "name": "São Tomé and Príncipe",
      "Country Name": "São Tomé and Príncipe",
      "Country Code": "STP"
   },
   {
      "country": "SA",
      "latitude": 23.885942,
      "longitude": 45.079162,
      "name": "Saudi Arabia",
      "Country Name": "Saudi Arabia",
      "Country Code": "SAU"
   },
   {
      "country": "SN",
      "latitude": 14.497401,
      "longitude": -14.452362,
      "name": "Senegal",
      "Country Name": "Senegal",
      "Country Code": "SEN"
   },
   {
      "country": "RS",
      "latitude": 44.016521,
      "longitude": 21.005859,
      "name": "Serbia",
      "Country Name": "Serbia",
      "Country Code": "SRB"
   },
   {
      "country": "SC",
      "latitude": -4.679574,
      "longitude": 55.491977,
      "name": "Seychelles",
      "Country Name": "Seychelles",
      "Country Code": "SYC"
   },
   {
      "country": "SL",
      "latitude": 8.460555,
      "longitude": -11.779889,
      "name": "Sierra Leone",
      "Country Name": "Sierra Leone",
      "Country Code": "SLE"
   },
   {
      "country": "SG",
      "latitude": 1.352083,
      "longitude": 103.819836,
      "name": "Singapore",
      "Country Name": "Singapore",
      "Country Code": "SGP"
   },
   {
      "Country Name": "Sint Maarten (Dutch part)",
      "Country Code": "SXM"
   },
   {
      "country": "SK",
      "latitude": 48.669026,
      "longitude": 19.699024,
      "name": "Slovakia",
      "Country Name": "Slovak Republic",
      "Country Code": "SVK"
   },
   {
      "country": "SI",
      "latitude": 46.151241,
      "longitude": 14.995463,
      "name": "Slovenia",
      "Country Name": "Slovenia",
      "Country Code": "SVN"
   },
   {
      "country": "SB",
      "latitude": -9.64571,
      "longitude": 160.156194,
      "name": "Solomon Islands",
      "Country Name": "Solomon Islands",
      "Country Code": "SLB"
   },
   {
      "country": "SO",
      "latitude": 5.152149,
      "longitude": 46.199616,
      "name": "Somalia",
      "Country Name": "Somalia",
      "Country Code": "SOM"
   },
   {
      "country": "ZA",
      "latitude": -30.559482,
      "longitude": 22.937506,
      "name": "South Africa",
      "Country Name": "South Africa",
      "Country Code": "ZAF"
   },
   {
      "country": "GS",
      "latitude": -54.429579,
      "longitude": -36.587909,
      "name": "South Georgia and the South Sandwich Islands"
   },
   {
      "country": "KR",
      "latitude": 35.907757,
      "longitude": 127.766922,
      "name": "South Korea",
      "Country Name": "South Korea",
      "Country Code": "KOR"
   },
   {
      "country": "ES",
      "latitude": 40.463667,
      "longitude": -3.74922,
      "name": "Spain",
      "Country Name": "Spain",
      "Country Code": "ESP"
   },
   {
      "country": "LK",
      "latitude": 7.873054,
      "longitude": 80.771797,
      "name": "Sri Lanka",
      "Country Name": "Sri Lanka",
      "Country Code": "LKA"
   },
   {
      "country": "SD",
      "latitude": 12.862807,
      "longitude": 30.217636,
      "name": "Sudan",
      "Country Name": "Sudan",
      "Country Code": "SDN"
   },
   {
      "Country Name": "South Sudan",
      "Country Code": "SSD"
   },
   {
      "country": "SR",
      "latitude": 3.919305,
      "longitude": -56.027783,
      "name": "Suriname",
      "Country Name": "Suriname",
      "Country Code": "SUR"
   },
   {
      "country": "SJ",
      "latitude": 77.553604,
      "longitude": 23.670272,
      "name": "Svalbard and Jan Mayen"
   },
   {
      "country": "SE",
      "latitude": 60.128161,
      "longitude": 18.643501,
      "name": "Sweden",
      "Country Name": "Sweden",
      "Country Code": "SWE"
   },
   {
      "Country Name": "St. Kitts and Nevis",
      "Country Code": "KNA"
   },
   {
      "Country Name": "St. Lucia",
      "Country Code": "LCA"
   },
   {
      "Country Name": "St. Martin (French part)",
      "Country Code": "MAF"
   },
   {
      "Country Name": "St. Vincent and the Grenadines",
      "Country Code": "VCT"
   },
   {
      "country": "CH",
      "latitude": 46.818188,
      "longitude": 8.227512,
      "name": "Switzerland",
      "Country Name": "Switzerland",
      "Country Code": "CHE"
   },
   {
      "country": "SY",
      "latitude": 34.802075,
      "longitude": 38.996815,
      "name": "Syria",
      "Country Name": "Syria",
      "Country Code": "SYR"
   },
   {
      "country": "TW",
      "latitude": 23.69781,
      "longitude": 120.960515,
      "name": "Taiwan"
   },
   {
      "country": "TJ",
      "latitude": 38.861034,
      "longitude": 71.276093,
      "name": "Tajikistan",
      "Country Name": "Tajikistan",
      "Country Code": "TJK"
   },
   {
      "country": "TZ",
      "latitude": -6.369028,
      "longitude": 34.888822,
      "name": "Tanzania",
      "Country Name": "Tanzania",
      "Country Code": "TZA"
   },
   {
      "country": "TH",
      "latitude": 15.870032,
      "longitude": 100.992541,
      "name": "Thailand",
      "Country Name": "Thailand",
      "Country Code": "THA"
   },
   {
      "country": "TL",
      "latitude": -8.874217,
      "longitude": 125.727539,
      "name": "Timor-Leste",
      "Country Name": "Timor-Leste",
      "Country Code": "TLS"
   },
   {
      "country": "TG",
      "latitude": 8.619543,
      "longitude": 0.824782,
      "name": "Togo",
      "Country Name": "Togo",
      "Country Code": "TGO"
   },
   {
      "country": "TK",
      "latitude": -8.967363,
      "longitude": -171.855881,
      "name": "Tokelau"
   },
   {
      "country": "TO",
      "latitude": -21.178986,
      "longitude": -175.198242,
      "name": "Tonga",
      "Country Name": "Tonga",
      "Country Code": "TON"
   },
   {
      "country": "TT",
      "latitude": 10.691803,
      "longitude": -61.222503,
      "name": "Trinidad and Tobago",
      "Country Name": "Trinidad and Tobago",
      "Country Code": "TTO"
   },
   {
      "country": "TN",
      "latitude": 33.886917,
      "longitude": 9.537499,
      "name": "Tunisia",
      "Country Name": "Tunisia",
      "Country Code": "TUN"
   },
   {
      "country": "TR",
      "latitude": 38.963745,
      "longitude": 35.243322,
      "name": "Turkey",
      "Country Name": "Turkey",
      "Country Code": "TUR"
   },
   {
      "country": "TM",
      "latitude": 38.969719,
      "longitude": 59.556278,
      "name": "Turkmenistan",
      "Country Name": "Turkmenistan",
      "Country Code": "TKM"
   },
   {
      "country": "TC",
      "latitude": 21.694025,
      "longitude": -71.797928,
      "name": "Turks and Caicos Islands",
      "Country Name": "Turks and Caicos Islands",
      "Country Code": "TCA"
   },
   {
      "country": "TV",
      "latitude": -7.109535,
      "longitude": 177.64933,
      "name": "Tuvalu",
      "Country Name": "Tuvalu",
      "Country Code": "TUV"
   },
   {
      "country": "UM",
      "name": "U.S. Minor Outlying Islands"
   },
   {
      "country": "VI",
      "latitude": 18.335765,
      "longitude": -64.896335,
      "name": "U.S. Virgin Islands",
      "Country Name": "Virgin Islands (U.S.)",
      "Country Code": "VIR"
   },
   {
      "country": "UG",
      "latitude": 1.373333,
      "longitude": 32.290275,
      "name": "Uganda",
      "Country Name": "Uganda",
      "Country Code": "UGA"
   },
   {
      "country": "UA",
      "latitude": 48.379433,
      "longitude": 31.16558,
      "name": "Ukraine",
      "Country Name": "Ukraine",
      "Country Code": "UKR"
   },
   {
      "country": "AE",
      "latitude": 23.424076,
      "longitude": 53.847818,
      "name": "United Arab Emirates",
      "Country Name": "United Arab Emirates",
      "Country Code": "ARE"
   },
   {
      "country": "GB",
      "latitude": 55.378051,
      "longitude": -3.435973,
      "name": "United Kingdom",
      "Country Name": "United Kingdom",
      "Country Code": "GBR"
   },
   {
      "country": "US",
      "latitude": 37.09024,
      "longitude": -95.712891,
      "name": "United States",
      "Country Name": "United States",
      "Country Code": "USA"
   },
   {
      "country": "UY",
      "latitude": -32.522779,
      "longitude": -55.765835,
      "name": "Uruguay",
      "Country Name": "Uruguay",
      "Country Code": "URY"
   },
   {
      "country": "UZ",
      "latitude": 41.377491,
      "longitude": 64.585262,
      "name": "Uzbekistan",
      "Country Name": "Uzbekistan",
      "Country Code": "UZB"
   },
   {
      "country": "VU",
      "latitude": -15.376706,
      "longitude": 166.959158,
      "name": "Vanuatu",
      "Country Name": "Vanuatu",
      "Country Code": "VUT"
   },
   {
      "country": "VA",
      "latitude": 41.902916,
      "longitude": 12.453389,
      "name": "Vatican City"
   },
   {
      "country": "VE",
      "latitude": 6.42375,
      "longitude": -66.58973,
      "name": "Venezuela",
      "Country Name": "Venezuela",
      "Country Code": "VEN"
   },
   {
      "country": "VN",
      "latitude": 14.058324,
      "longitude": 108.277199,
      "name": "Vietnam",
      "Country Name": "Vietnam",
      "Country Code": "VNM"
   },
   {
      "country": "WF",
      "latitude": -13.768752,
      "longitude": -177.156097,
      "name": "Wallis and Futuna"
   },
   {
      "Country Name": "West Bank and Gaza",
      "Country Code": "PSE"
   },
   {
      "country": "EH",
      "latitude": 24.215527,
      "longitude": -12.885834,
      "name": "Western Sahara"
   },
   {
      "country": "YE",
      "latitude": 15.552727,
      "longitude": 48.516388,
      "name": "Yemen",
      "Country Name": "Yemen",
      "Country Code": "YEM"
   },
   {
      "country": "ZM",
      "latitude": -13.133897,
      "longitude": 27.849332,
      "name": "Zambia",
      "Country Name": "Zambia",
      "Country Code": "ZMB"
   },
   {
      "country": "ZW",
      "latitude": -19.015438,
      "longitude": 29.154857,
      "name": "Zimbabwe",
      "Country Name": "Zimbabwe",
      "Country Code": "ZWE"
   }
]